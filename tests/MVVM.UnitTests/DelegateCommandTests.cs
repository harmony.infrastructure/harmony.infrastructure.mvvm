﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.ComponentModel;

namespace Harmony.Infrastructure.MVVM.UnitTests
{
	[TestFixture]
	public class DelegateCommandTests
	{
		[Test]
		public void ExecuteActionFromConstructor()
		{
			#region Arrange
			
			var commandTesting = new CommandTesting();
			var testingObject = new DelegateCommand(commandTesting.Execute, commandTesting.CanExecute);

			#endregion

			#region Act

			testingObject.Execute(1);
			testingObject.CanExecute(2);

			#endregion

			#region Assert

			Assert.That(commandTesting.ExecuteCalls.Count, Is.EqualTo(1), "Quantity occurred calls Execute is not correct");
			Assert.That(commandTesting.CanExecuteCalls.Count, Is.EqualTo(1), "Quantity occurred calls CanExecute is not correct");
			Assert.That((int)commandTesting.ExecuteCalls[0], Is.EqualTo(1), "Parameter value in occurred call Execute is not correct");
			Assert.That((int)commandTesting.CanExecuteCalls[0], Is.EqualTo(2), "Parameter value in occurred call CanExecute is not correct");

			#endregion
		}

		#region Classes

		public class CommandTesting : BindableViewModel
		{
			public CommandTesting()
			{
				ExecuteCalls = new List<object>();
				CanExecuteCalls = new List<object>();
			}

			#region Methods

			public void Execute(object parameter)
			{
				ExecuteCalls.Add(parameter);
			}

			public bool CanExecute(object parameter)
			{
				CanExecuteCalls.Add(parameter);
				return true;
			}

			#endregion

			#region Properties

			public List<object> ExecuteCalls { get; set; }

			public List<object> CanExecuteCalls { get; set; }

			#endregion
		}

		#endregion
	}
}
