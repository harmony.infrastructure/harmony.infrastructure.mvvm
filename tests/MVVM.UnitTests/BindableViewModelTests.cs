﻿using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.ComponentModel;

namespace Harmony.Infrastructure.MVVM.UnitTests
{
	[TestFixture]
	public class BindableViewModelTests
	{
		[Test]
		public void ChangeProperty_RaiseEvent()
		{
			#region Arrange

			var testingObject = new BindableViewModelTesting();
			var eventTesting = new EventTesting<PropertyChangedEventArgs>();
			
			testingObject.PropertyChanged += eventTesting.Raise;

			#endregion

			#region Act

			testingObject.StringProperty = "TestValue";

			#endregion

			#region Assert

			Assert.That(eventTesting.OccurredEvents.Count, Is.EqualTo(1), "Quantity occurred events is not correct");
			Assert.That(eventTesting.OccurredEvents[0].PropertyName, Is.EqualTo(nameof(testingObject.StringProperty)), "Property name in occurred event is not correct");
			Assert.That(testingObject.StringProperty, Is.EqualTo("TestValue"), "Property value is not correct");

			#endregion
		}

		[Test]
		public void RepeatPropertyValue_RaiseEventOnlyOne()
		{
			#region Arrange

			var testingObject = new BindableViewModelTesting();
			var eventTesting = new EventTesting<PropertyChangedEventArgs>();

			testingObject.PropertyChanged += eventTesting.Raise;

			#endregion

			#region Act

			testingObject.StringProperty = "TestValue";
			testingObject.StringProperty = "TestValue";

			#endregion

			#region Assert

			Assert.That(eventTesting.OccurredEvents.Count, Is.EqualTo(1), "Quantity occurred events is not correct");
			Assert.That(eventTesting.OccurredEvents[0].PropertyName, Is.EqualTo(nameof(testingObject.StringProperty)), "Property name in occurred event is not correct");
			Assert.That(testingObject.StringProperty, Is.EqualTo("TestValue"), "Property value is not correct");

			#endregion
		}

		[Test]
		public void ChangeProperty_RaiseEventForDependentProperty()
		{
			#region Arrange

			var testingObject = new BindableViewModelTesting();
			var eventTesting = new EventTesting<PropertyChangedEventArgs>();

			testingObject.PropertyChanged += eventTesting.Raise;

			#endregion

			#region Act

			testingObject.MainProperty = "TestValue";

			#endregion

			#region Assert

			Assert.That(eventTesting.OccurredEvents.Count, Is.EqualTo(2), "Quantity occurred events is not correct");
			Assert.That(eventTesting.OccurredEvents[0].PropertyName, Is.EqualTo(nameof(testingObject.MainProperty)), "Main Property name in occurred event is not correct");
			Assert.That(eventTesting.OccurredEvents[1].PropertyName, Is.EqualTo(nameof(testingObject.DependentProperty)), "Dependent Property name in occurred event is not correct");
			Assert.That(testingObject.MainProperty, Is.EqualTo("TestValue"), "Property value is not correct");

			#endregion
		}

		[Test]
		public void ChangeProperty_RaiseEventForDependentCommand()
		{
			#region Arrange

			var testingObject = new BindableViewModelTesting();
			var eventPropertyChangedTesting = new EventTesting<PropertyChangedEventArgs>();
			var eventCanExecuteChangedTesting = new EventTesting<EventArgs>();

			testingObject.PropertyChanged += eventPropertyChangedTesting.Raise;
			testingObject.TestCommand.CanExecuteChanged += eventCanExecuteChangedTesting.Raise;

			#endregion

			#region Act

			var canExecuteBefore = testingObject.TestCommand.CanExecute(null);
			testingObject.CommandProperty = "TestValue";
			var canExecuteAfter = testingObject.TestCommand.CanExecute(null);

			#endregion

			#region Assert

			Assert.That(eventPropertyChangedTesting.OccurredEvents.Count, Is.EqualTo(1), "Quantity occurred events PropertyChanged is not correct");
			Assert.That(eventPropertyChangedTesting.OccurredEvents[0].PropertyName, Is.EqualTo(nameof(testingObject.CommandProperty)), "Property name in occurred event is not correct");
			Assert.That(testingObject.CommandProperty, Is.EqualTo("TestValue"), "Property value is not correct");

			Assert.That(eventCanExecuteChangedTesting.OccurredEvents.Count, Is.EqualTo(1), "Quantity occurred events CanExecuteChanged is not correct");
			Assert.That(canExecuteBefore, Is.False, "CanExecuteBefore is not correct");
			Assert.That(canExecuteAfter, Is.True, "CanExecuteAfter is not correct");

			#endregion
		}

		[Test]
		public void SetWrongProperty_ThrowError()
		{
			#region Arrange

			var testingObject = new BindableViewModelTesting();
			var eventTesting = new EventTesting<PropertyChangedEventArgs>();

			testingObject.PropertyChanged += eventTesting.Raise;

			#endregion

			#region Assert

			var exception = Assert.Throws<ArgumentNullException>(delegate
			{
				testingObject.WrongProperty = "TestValue";
			}, "WrongProperty did not throw error");

			Assert.That(exception, Is.Not.Null, "Exception is empty");
			Assert.That(exception.ParamName, Is.EqualTo("propertyName"),
				"Exception.ParamName is invalid");

			#endregion
		}

		[Test]
		public void GetWrongProperty_ThrowError()
		{
			#region Arrange

			var testingObject = new BindableViewModelTesting();
			var eventTesting = new EventTesting<PropertyChangedEventArgs>();

			testingObject.PropertyChanged += eventTesting.Raise;
			
			#endregion

			#region Assert

			var exception = Assert.Throws<ArgumentNullException>(delegate
			{
				var actualValue = testingObject.WrongProperty;
				
				Assert.That(actualValue, Is.Null, "Property value is invalid");
			}, "WrongProperty did not throw error");

			Assert.That(exception, Is.Not.Null, "Exception is empty");
			Assert.That(exception.ParamName, Is.EqualTo("propertyName"),
				"Exception.ParamName is invalid");
			
			#endregion
		}

		#region Classes

		public class BindableViewModelTesting : BindableViewModel
		{
			public BindableViewModelTesting()
			{
				TestCommand = new DelegateCommand(TestCommandExecute, TestCommandCanExecute);
			}

			public string WrongProperty
			{
				get => GetPropertyValue<string>(String.Empty);
				set => SetPropertyValue(value, String.Empty);
			}

			public string StringProperty
			{
				get => GetPropertyValue<string>(nameof(StringProperty));
				set => SetPropertyValue(value, nameof(StringProperty));
			}

			public string MainProperty
			{
				get => GetPropertyValue<string>(nameof(MainProperty));
				set => SetPropertyValue(value, nameof(MainProperty));
			}

			[DependencyFromProperty(nameof(MainProperty))]
			public string DependentProperty { get; set; }

			public string CommandProperty
			{
				get => GetPropertyValue<string>(nameof(CommandProperty));
				set => SetPropertyValue(value, nameof(CommandProperty));
			}

			[DependencyFromProperty(nameof(CommandProperty))]
			public DelegateCommand TestCommand { get; } 

			private void TestCommandExecute(object parameter)
			{

			}

			private bool TestCommandCanExecute(object parameter)
			{
				return !String.IsNullOrEmpty(CommandProperty);
			}
		}

		#endregion
	}
}
