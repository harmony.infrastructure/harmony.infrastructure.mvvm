﻿using System;
using Harmony.Infrastructure.MVVM;

namespace MVVM.IntegrationTests.WPF_NET40
{
	public class MainWindowViewModel : ViewModelBase
	{
		public MainWindowViewModel()
		{
			ValidationRules.Add(nameof(TestProperty), ValidateProperty);
		}

		public string TestProperty
		{
			get => GetPropertyValue<string>(nameof(TestProperty));
			set => SetPropertyValue(value, nameof(TestProperty));
		}

		private void ValidateProperty()
		{
			var propertyName = nameof(TestProperty);

			if (String.IsNullOrEmpty(TestProperty))
				AddError(propertyName, "Value is empty");
			else if (TestProperty.Length < 3)
				AddError(propertyName, "Value is less than 3 characters");
		}
	}
}

