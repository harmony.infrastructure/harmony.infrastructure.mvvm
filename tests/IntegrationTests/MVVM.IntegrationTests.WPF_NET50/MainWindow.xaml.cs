﻿using System.Windows;

namespace MVVM.IntegrationTests.WPF_NET50
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			var viewModel = new MainWindowViewModel();
			DataContext = viewModel;
			InitializeComponent();
			viewModel.Initialize();
		}

	}
}
