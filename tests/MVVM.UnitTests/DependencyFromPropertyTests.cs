﻿using System;
using NUnit.Framework;

namespace Harmony.Infrastructure.MVVM.UnitTests
{
	[TestFixture]
	public class DependencyFromPropertyTests
	{
		[Test]
		public void SetWrongAttribute_ThrowError()
		{
			#region Assert

			var exception = Assert.Throws<ArgumentNullException>(delegate
			{
				var testingObject = new DependencyFromPropertyTesting();
				testingObject.WrongProperty = "TestValue";
			}, "WrongProperty did not throw error");

			Assert.That(exception, Is.Not.Null, "Exception is empty");
			Assert.That(exception.ParamName, Is.EqualTo("propertyName"),
				"Exception.ParamName is invalid");

			#endregion
		}
		
		#region Classes

		public class DependencyFromPropertyTesting : BindableViewModel
		{
			[DependencyFromProperty("")]
			public string WrongProperty
			{
				get => GetPropertyValue<string>(nameof(WrongProperty));
				set => SetPropertyValue(value, nameof(WrongProperty));
			}
		}
		
		#endregion

	}
}
