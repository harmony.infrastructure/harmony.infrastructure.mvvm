﻿using System;

namespace Harmony.Infrastructure.MVVM.UnitTests
{
	public class ViewModelBaseTesting : ViewModelBase
	{
		public ViewModelBaseTesting()
		{
			ValidationRules.Add(nameof(StringProperty), ValidateStringProperty);
			ValidationRules.Add(nameof(Age), ValidateAge);
		}
		
		public string StringProperty
		{
			get => GetPropertyValue<string>(nameof(StringProperty));
			set => SetPropertyValue(value, nameof(StringProperty));
		}

		public int Age
		{
			get => GetPropertyValue<int>(nameof(Age));
			set => SetPropertyValue(value, nameof(Age));
		}

		private void ValidateStringProperty()
		{
			var propertyName = nameof(StringProperty);

			if (String.IsNullOrEmpty(StringProperty))
				AddError(propertyName, "Property is empty");
			else if (StringProperty.Length < 5)
				AddError(propertyName, "Property is less than 5 symbols");
		}

		private void ValidateAge()
		{
			var propertyName = nameof(Age);

			if (Age < 20 || Age > 50)
			{
				AddError(propertyName, "Invalid age");
				AddError(propertyName, Age < 20
					? "You are very young"
					: "You are very old");
			}
		}
	}
}
