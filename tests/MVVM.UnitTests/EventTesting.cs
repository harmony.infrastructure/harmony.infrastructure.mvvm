﻿using System;
using System.Collections.Generic;

namespace Harmony.Infrastructure.MVVM.UnitTests
{
	public class EventTesting<T>
		where T : EventArgs
	{
		public EventTesting()
		{
			OccurredEvents = new List<T>();
		}

		#region Methods

		public void Raise(object sender, T args)
		{
			OccurredEvents.Add(args);
		}

		#endregion

		#region Properties

		public List<T> OccurredEvents { get; }

		#endregion
	}
}
