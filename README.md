# Harmony.Infrastructure.MVVM

Contains code helpers for WPF with MVVM pattern

## BindableViewModel
Implementation INotifyPropertyChanged and INotifyPropertyChanging
Get started:

	public class NewClassViewModel : BindableViewModel
	{
		public string NewProperty
		{
			get => GetPropertyValue<string>(nameof(NewProperty)); 
			set => SetPropertyValue(value, nameof(NewProperty));
		}

		// When NewProperty is changed then DependencyFromProperty will raise PropertyChanged for SecondProperty
		[DependencyFromProperty(nameof(NewProperty))]
		public string SecondProperty
		{
			get => GetPropertyValue<string>(nameof(SecondProperty)); 
			set => SetPropertyValue(value, nameof(SecondProperty));
		}
	}

## DelegateCommand
Implementation ICommand

	public class NewClassViewModel : BindableViewModel
	{
		public NewClassViewModel()
		{
			TestCommand = new DelegateCommand(TestCommandExecuted, TestCommandCanExecuted);
		}

		public string NewProperty
		{
			get => GetPropertyValue<string>(nameof(NewProperty)); 
			set => SetPropertyValue(value, nameof(NewProperty));
		}

		// When NewProperty is changed then DependencyFromProperty will raise CanExecuteChanged for command
		[DependencyFromProperty(nameof(NewProperty))]
		public DelegateCommand TestCommand { get; }

		private async void TestCommandExecuted(object parameter)
		{
			//...
		}

		private bool TestCommandCanExecuted(object parameter)
		{
			//...
		}
	}

## INotifyDataErrorInfo

ViewModelBase implemented INotifyDataErrorInfo

	<TextBox Grid.Row="0"
	         Grid.Column="0"
	         Width="100"
	         Height="20"
	         Name="TestPropertyTextBox"
	         Text="{Binding Path=TestProperty, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnNotifyDataErrors=True}" />

	<TextBlock Grid.Row="0"
		       Grid.Column="1"
		       Width="100"
		       Text="{Binding ElementName=TestPropertyTextBox, Path=(Validation.Errors)[0].ErrorContent}"
		       Foreground="Red" />

	public class MainWindowViewModel : ViewModelBase
	{
		public MainWindowViewModel()
		{
			ValidationRules.Add(nameof(TestProperty), ValidateProperty);
		}

		public string TestProperty
		{
			get => GetPropertyValue<string>(nameof(TestProperty));
			set => SetPropertyValue(value, nameof(TestProperty));
		}

		private void ValidateProperty()
		{
			var propertyName = nameof(TestProperty);

			if (String.IsNullOrEmpty(TestProperty))
				AddError(propertyName, "Value is empty");
			else if (TestProperty.Length < 5)
				AddError(propertyName, "Value is less than 5 characters");
		}
	}
