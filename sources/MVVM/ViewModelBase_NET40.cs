﻿#if FEATURE_NET40

using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Harmony.Infrastructure.MVVM
{
	public abstract class ViewModelBase : BindableViewModel, IDataErrorInfo
	{
		#region Fields

		private readonly Dictionary<string, List<string>> _propertyErrors;

		#endregion

		protected ViewModelBase()
		{
			_propertyErrors = new Dictionary<string, List<string>>();
			ValidationRules = new Dictionary<string, Action>();
		}

		#region Methods

		public virtual void Initialize()
		{

		}

		/// <summary>
		/// Call specific validation rule
		/// </summary>
		/// <param name="propertyName"></param>
		private void ValidateProperty(string propertyName)
		{
			if (ValidationRules.ContainsKey(propertyName))
				ValidationRules[propertyName].Invoke();
		}

		protected void AddError(string propertyName, string error)
		{
			List<string> errors;

			if (_propertyErrors.ContainsKey(propertyName))
				errors = _propertyErrors[propertyName];
			else
			{
				errors = new List<string>();
				_propertyErrors[propertyName] = errors;
			}

			errors.Add(error);
		}

		private void ClearPropertyErrors(string propertyName)
		{
			if (_propertyErrors.ContainsKey(propertyName))
				_propertyErrors.Remove(propertyName);
		}

		#endregion

		#region Properties

		public Dictionary<string, Action> ValidationRules { get; }

		#endregion

		#region Implementaion IDataErrorInfo

		public string this[string propertyName]
		{
			get
			{
				ClearPropertyErrors(propertyName);
				ValidateProperty(propertyName);

				if (String.IsNullOrEmpty(propertyName))
					return String.Empty;

				if (_propertyErrors.ContainsKey(propertyName))
				{
					var errors = _propertyErrors[propertyName];

					var errorText = String.Join(Environment.NewLine, errors);

					return errorText;
				}

				return String.Empty;
			}
		}

		public string Error => throw new NotImplementedException();

		#endregion
	}
}

#endif