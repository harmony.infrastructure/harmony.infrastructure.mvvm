﻿#if !FEATURE_NET40

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Harmony.Infrastructure.MVVM
{
	public abstract class ViewModelBase : BindableViewModel, INotifyDataErrorInfo
	{
		#region Fields

		private readonly Dictionary<string, List<string>> _propertyErrors;

		#endregion

		protected ViewModelBase()
		{
			_propertyErrors = new Dictionary<string, List<string>>();
			ValidationRules = new Dictionary<string, Action>();

			PropertyChanged += ViewModelBase_PropertyChanged;
		}

		#region Methods

		public virtual void Initialize()
		{
			// Call all validations
			foreach (var validationRule in ValidationRules.Values)
				validationRule.Invoke();
		}

		/// <summary>
		/// Call specific validation rule, auto called when raised <see cref="PropertyChangedEventHandler"/>
		/// </summary>
		/// <param name="propertyName"></param>
		private void ValidateProperty(string propertyName)
		{
			if (ValidationRules.ContainsKey(propertyName))
				ValidationRules[propertyName].Invoke();
		}

		
		protected void AddError(string propertyName, string error)
		{
			List<string> errors;

			if (_propertyErrors.ContainsKey(propertyName))
				errors = _propertyErrors[propertyName];
			else
			{
				errors = new List<string>();
				_propertyErrors[propertyName] = errors;
			}

			errors.Add(error);
			OnErrorsChanged(propertyName);
		}

		private void ClearPropertyErrors(string propertyName)
		{
			if (_propertyErrors.ContainsKey(propertyName))
			{
				_propertyErrors.Remove(propertyName);
				OnErrorsChanged(propertyName);
			}
		}

		#endregion

		#region Properties

		public Dictionary<string, Action> ValidationRules { get; }

		#endregion

		#region Events Handlers

		private void ViewModelBase_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			ClearPropertyErrors(e.PropertyName);
			ValidateProperty(e.PropertyName);
		}

		#endregion

		#region Implementaion INotifyDataErrorInfo

		public bool HasErrors => _propertyErrors.Count > 0;

		public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

		private void OnErrorsChanged(string propertyName)
		{
			ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
		}

		public IEnumerable GetErrors(string propertyName)
		{
			if (String.IsNullOrEmpty(propertyName))
				return null;

			return _propertyErrors.ContainsKey(propertyName)
				? _propertyErrors[propertyName]
				: null;
		}

		#endregion
	}
}

#endif