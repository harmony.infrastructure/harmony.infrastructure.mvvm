﻿#if FEATURE_NET40

using NUnit.Framework;
using System;

namespace Harmony.Infrastructure.MVVM.UnitTests
{

	[TestFixture]
	public class ViewModelBaseTests
	{
		[Test]
		public void ChangeProperty_RaiseError()
		{
			#region Arrange

			var testingObject = new ViewModelBaseTesting();
			testingObject.Initialize();

			#endregion

			#region Act

			testingObject.StringProperty = "Test";

			#endregion

			#region Assert

			Assert.That(testingObject[nameof(testingObject.StringProperty)],
				Is.EqualTo("Property is less than 5 symbols"), "Error text is not correct");

			#endregion
		}

		[Test]
		public void ChangeProperty_ShowSomeErrors()
		{
			#region Arrange

			var testingObject = new ViewModelBaseTesting();
			testingObject.Initialize();

			#endregion

			#region Act

			testingObject.Age = 9;

			#endregion

			#region Assert

			Assert.That(testingObject[nameof(testingObject.Age)],
				Is.EqualTo($"Invalid age{Environment.NewLine}You are very young"), "Error text is not correct");

			#endregion
		}

		[Test]
		public void ChangePropertyTwice_ShowLastError()
		{
			#region Arrange

			var testingObject = new ViewModelBaseTesting();
			testingObject.Initialize();

			#endregion

			#region Act

			testingObject.StringProperty = "Test";
			var firstError = testingObject[nameof(testingObject.StringProperty)];

			testingObject.StringProperty = String.Empty;
			var secondError = testingObject[nameof(testingObject.StringProperty)];

			#endregion

			#region Assert

			Assert.That(firstError,
				Is.EqualTo("Property is less than 5 symbols"), "Error text is not correct");

			Assert.That(secondError,
				Is.EqualTo("Property is empty"), "Error text is not correct");

			#endregion
		}

		[Test]
		public void GetErrors_UnknownProperty_ReturnEmpty()
		{
			#region Arrange

			var testingObject = new ViewModelBaseTesting();
			testingObject.Initialize();

			#endregion

			#region Act

			testingObject.StringProperty = "Test";
			var errors_UniknownProperty = testingObject["UNKNOWN"];
			var errors_EmptyPropertyName = testingObject[String.Empty];

			#endregion

			#region Assert

			Assert.That(errors_UniknownProperty, Is.EqualTo(String.Empty), "Error is is not correct");
			Assert.That(errors_EmptyPropertyName, Is.EqualTo(String.Empty), "Error is is not correct");

			#endregion
		}

		[Test]
		public void Error_ThrowNotImplemented()
		{
			#region Act

			var testingObject = new ViewModelBaseTesting();

			#endregion

			#region Assert

			var exception = Assert.Throws<NotImplementedException>(delegate
			{
				var actualError = testingObject.Error;
				
				Assert.That(actualError, Is.Null, "Error has value");
				Assert.That(actualError, Is.Not.Null, "Error has not value");
			}, "Property Error did not throw error");

			Assert.That(exception, Is.Not.Null, "Exception is empty");

			#endregion
		}
	}
}

#endif