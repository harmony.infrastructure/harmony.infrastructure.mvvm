﻿#if !FEATURE_NET40

using System;
using NUnit.Framework;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Harmony.Infrastructure.MVVM.UnitTests
{

	[TestFixture]
	public class ViewModelBaseTests
	{
		[Test]
		public void ChangeProperty_ShowRaise()
		{
			#region Arrange

			var testingObject = new ViewModelBaseTesting();
			var eventTesting = new EventTesting<DataErrorsChangedEventArgs>();

			testingObject.ErrorsChanged += eventTesting.Raise;
			testingObject.Initialize();

			#endregion

			#region Act

			testingObject.StringProperty = "Test";

			#endregion

			#region Assert

			var occurredEvents = eventTesting.OccurredEvents
				.Where(item => item.PropertyName == nameof(testingObject.StringProperty))
				.ToArray();

			Assert.That(occurredEvents.Length, Is.EqualTo(3), "Quantity occurred events is not correct");
			Assert.That(testingObject.HasErrors, Is.True, "HasErrors is not correct");

			var error = (testingObject.GetErrors(eventTesting.OccurredEvents[0].PropertyName) as IEnumerable<string>).FirstOrDefault();

			Assert.That(error, Is.EqualTo("Property is less than 5 symbols"), "Error text is not correct");

			#endregion
		}

		[Test]
		public void ChangeProperty_ShowSomeErrors()
		{
			#region Arrange

			var testingObject = new ViewModelBaseTesting();
			var eventTesting = new EventTesting<DataErrorsChangedEventArgs>();

			testingObject.ErrorsChanged += eventTesting.Raise;
			testingObject.Initialize();

			#endregion

			#region Act

			testingObject.Age = 9;

			#endregion

			#region Assert

			var occurredEvents = eventTesting.OccurredEvents
				.Where(item => item.PropertyName == nameof(testingObject.Age))
				.ToArray();

			Assert.That(occurredEvents.Length, Is.EqualTo(5), "Quantity occurred events is not correct");
			Assert.That(testingObject.HasErrors, Is.True, "HasErrors is not correct");

			var errors = testingObject.GetErrors(nameof(testingObject.Age)) as IEnumerable<string>;

			var errorText = String.Join(Environment.NewLine, errors);

			Assert.That(errorText, Is.EqualTo($"Invalid age{Environment.NewLine}You are very young"), "Error text is not correct");

			#endregion
		}

		[Test]
		public void ChangePropertyTwice_ShowLastError()
		{
			#region Arrange

			var testingObject = new ViewModelBaseTesting();

			var eventTesting = new EventTesting<DataErrorsChangedEventArgs>();

			testingObject.ErrorsChanged += eventTesting.Raise;
			testingObject.Initialize();

			#endregion

			#region Act

			testingObject.StringProperty = "Test";
			testingObject.StringProperty = String.Empty;

			#endregion

			#region Assert

			var occurredEvents = eventTesting.OccurredEvents
				.Where(item => item.PropertyName == nameof(testingObject.StringProperty))
				.ToArray();

			Assert.That(occurredEvents.Length, Is.EqualTo(5), "Quantity occurred events is not correct");
			Assert.That(testingObject.HasErrors, Is.True, "HasErrors is not correct");

			var error = (testingObject.GetErrors(eventTesting.OccurredEvents[0].PropertyName) as IEnumerable<string>).FirstOrDefault();

			Assert.That(error, Is.EqualTo("Property is empty"), "Error text is not correct");

			#endregion
		}

		[Test]
		public void GetErrors_UnknownProperty_ReturnNull()
		{
			#region Arrange

			var testingObject = new ViewModelBaseTesting();
			testingObject.Initialize();
			
			#endregion

			#region Act

			testingObject.StringProperty = "Test";
			var errors_UniknownProperty = testingObject.GetErrors("UNKNOWN");
			var errors_EmptyPropertyName = testingObject.GetErrors(null);

			#endregion

			#region Assert

			Assert.That(errors_UniknownProperty, Is.Null, "Errors list is is not correct");
			Assert.That(errors_EmptyPropertyName, Is.Null, "Errors list is is not correct");

			#endregion
		}
	}
}

#endif